﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;

namespace CustomAttributes.Models
{
    public class ShoppingCart
    {
        public Costumer Costumer { get; }
        public List<ChosenItem> Items;

        [Logger("ShoppingCart Constructor")]
        public ShoppingCart(Costumer costumer)
        {
            Costumer = costumer;
            Items = new List<ChosenItem>();
        }

        [Logger("AddItemToCart")]
        public void AddItemToCart(ChosenItem item)
        {
            Items.Add(item);
        }

        [Logger("RemoveItemFromCart")]
        public void RemoveItemFormCart(ChosenItem item)
        {
            Items.Remove(item);
        }

        [Logger("Pay")]
        public void Pay()
        {
            Items = null;
        }

        [Logger("GetItems")]
        public List<ChosenItem> GetItems()
        {
            return Items;
        }
    }
}
