﻿namespace CustomAttributes.Models
{
    public class ChosenItem
    {
        public ShopItem ShopItem { get; }
        public int Count { get; }
        public int TotalPrice { get; }

        public ChosenItem(ShopItem shopItem, int count)
        {
            ShopItem = shopItem;
            Count = count;
            TotalPrice = shopItem.Good.Price * count;
        }
    }
}
