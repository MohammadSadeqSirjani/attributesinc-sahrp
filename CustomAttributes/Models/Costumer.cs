﻿namespace CustomAttributes.Models
{
    public class Costumer
    {
        public int CostumerId { get; }
        public string Firstname { get; }
        public string Surname { get; }

        public Costumer(int costumerId, string firstname, string surname)
        {
            CostumerId = costumerId;
            Firstname = firstname;
            Surname = surname;
        }
    }
}
