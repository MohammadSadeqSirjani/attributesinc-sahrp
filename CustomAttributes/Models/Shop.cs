﻿namespace CustomAttributes.Models
{
    public class Shop
    {
        public int ShopId { get; }
        public string Name { get; }

        public Shop(int shopId, string name)
        {
            ShopId = shopId;
            Name = name;
        }
    }
}
