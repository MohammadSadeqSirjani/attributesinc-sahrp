﻿namespace CustomAttributes.Models
{
    public class ShopItem
    {
        public Good Good { get; }
        public Shop Shop { get; }

        public ShopItem(Good good, Shop shop)
        {
            Good = good;
            Shop = shop;
        }
    }
}
