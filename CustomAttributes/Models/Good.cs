﻿namespace CustomAttributes.Models
{
    public class Good
    {
        public int GoodId { get; }
        public string Name { get; }
        public int Price { get; }

        public Good(int goodId, string name, int price)
        {
            GoodId = goodId;
            Name = name;
            Price = price;
        }
    }
}
