﻿using System;
using System.Linq;
using CustomAttributes.Models;
using BindingFlags = System.Reflection.BindingFlags;

namespace CustomAttributes
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            LoggerAttribute.Log(typeof(ShoppingCart));
        }
    }
}
