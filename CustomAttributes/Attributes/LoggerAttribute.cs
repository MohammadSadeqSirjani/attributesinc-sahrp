﻿using System;
using System.Linq;
using System.Reflection;
using CustomAttributes.Models;

namespace CustomAttributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Constructor)]
    public class LoggerAttribute : Attribute
    {
        public virtual string Message { get; }

        public LoggerAttribute(string message)
        {
            Message = message ?? throw new ArgumentNullException(nameof(Message));
        }

        public static void Log(Type cls)
        {
            Console.WriteLine($"Methods of class {cls}");

            var methodsInfo = cls
                .GetMethods(BindingFlags.Default |
                            BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly |
                            BindingFlags.Instance | BindingFlags.Static |
                            BindingFlags.Public | BindingFlags.NonPublic)
                .Select(m =>
                    new { Message = m.Name, MethodAttributes = m.GetCustomAttributes<LoggerAttribute>() })
                .SelectMany(m => m.MethodAttributes
                    .Select(a => new { MethodAttribute = a, Name = m.Message }));

            foreach (var method in methodsInfo)
                Console.WriteLine($"Method [{method.Name}] : {method.MethodAttribute.Message}");

            var constructorsInfo = cls.GetConstructors()
                .Select(m
                    => new { Message = m.Name, ConstrucotrAttributes = m.GetCustomAttributes<LoggerAttribute>() })
                .SelectMany(m => m.ConstrucotrAttributes
                    .Select(a => new { ConstructorAttribute = a, Name = m.Message }));

            foreach (var constructor in constructorsInfo)
                Console.WriteLine($"Constructor [{constructor.Name}] : {constructor.ConstructorAttribute.Message}");
        }
    }
}
