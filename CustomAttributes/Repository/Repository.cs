﻿using System.Collections.Generic;
using CustomAttributes.Models;

namespace CustomAttributes
{
    public class Repository
    {
        public List<Shop> Shops { private get; set; }
        public List<Good> Goods { get; set; }
        public List<ShopItem> ShopItems { private get; set; }
        public List<Costumer> Costumers { get; set; }
        public List<ShoppingCart> ShoppingCarts { get; set; }
        public List<ChosenItem> ChosenItems { get; set; }

        public Repository()
        {
            Shops = new List<Shop>
            {
                new Shop(1, "DigiKala"),
                new Shop(2, "MashhadKala"),
                new Shop(3, "Torob"),
                new Shop(4, "FarsKala")
            };

            Goods = new List<Good>()
            {
                new Good(1, "Ring", 2300),
                new Good(2, "Carpet", 8500),
                new Good(3, "TV", 1250),
                new Good(4, "Car", 5400),
            };

            ShopItems = new List<ShopItem>()
            {
                new ShopItem(Goods[1], Shops[0]),
                new ShopItem(Goods[2], Shops[2]),
                new ShopItem(Goods[3], Shops[3]),
                new ShopItem(Goods[2], Shops[1]),
                new ShopItem(Goods[2], Shops[3]),
                new ShopItem(Goods[0], Shops[2]),
                new ShopItem(Goods[3], Shops[0]),
                new ShopItem(Goods[3], Shops[1])
            };

            Costumers = new List<Costumer>()
            {
                new Costumer(1, "Sadeq", "Sirjani"),
                new Costumer(3, "Sadra", "Sirjani"),
                new Costumer(3, "Majid", "Sirjani"),
                new Costumer(4, "Fatemeh", "Shafiee")
            };

            ShoppingCarts = new List<ShoppingCart>()
            {
                new ShoppingCart(Costumers[0]),
                new ShoppingCart(Costumers[1]),
                new ShoppingCart(Costumers[2]),
                new ShoppingCart(Costumers[3]),
            };

            ChosenItems = new List<ChosenItem>()
            {
                new ChosenItem(ShopItems[0], 2),
                new ChosenItem(ShopItems[0], 1),
                new ChosenItem(ShopItems[0], 3),
                new ChosenItem(ShopItems[6], 2),
                new ChosenItem(ShopItems[5], 1),
                new ChosenItem(ShopItems[1], 1),
                new ChosenItem(ShopItems[3], 3),
                new ChosenItem(ShopItems[7], 2),
            };
        }
    }
}
