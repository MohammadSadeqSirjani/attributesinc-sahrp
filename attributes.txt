======================================================
**: note | -: subnote | ex: example | *: point
======================================================


** Undrestanding Attribute Contructors and Properties
- Required costructor parameters
- Optional contructor parameters
- Additional attribute properties
ex:
[Display(42.5)] //Requires parameter
[Display(42.5, 0.5)] // Optional parameter
[Display(42.5, precision: 0.5)] // Optional parameter
[Display(value: 42.5, precision: 0.5)]
[Display(42.5, 0.5, Prefix = "x", Postfix = "y")]

* Attributes can also define multiple constructor overloads as with regular class definitions

** Controlling the debugging experience
- [DebuggerDisplay] //
- [DebugerTypeProxy]
- [DebuggerBrowable] //

** Make code as deprecated
 - [Obsolete] // Compilation warnings and errors

** Conditionaly compiling code
- [Conditoinal]
Condtitional compilation symbols
// DEBUG 
 - [Conditional(mode)]
mode:
- DEBUG
- NETCORE
- NETCORE3-1
- TRACE

* Applying attributes at assembly level
ex: assembly levelmetadata
- Title
- Version
- Copyright
- Etc.

use assembly keyword
[assembly: AssemblyCopyright("copyright")]

** Exposing interanl code
[InternalVisibleTo]
Often use this in testing mode to adding modified 
setting to projects

**Applying attributes to return values
ex: Azure functionsbindings
return keywords

** When we want to force other programmer to use specific attributes in specific place
 use [AttributeUsage(AttributeTarget.*)]
 I tell the story below: 
*Attribute Targets
- All
- Assembly
- Class
- Contructor
- Delegate
- Enum
- Field
- GenericParameter
- Interface
- Method
- Module
- Parameter
- Property
- ReturnValue
- struct
ex:
if you want to use this attribute for all sitiations:
[AttributeUsage(AttributeTargetes.All)]
if you want to use this attribute in special postion:
[AttributeUsage(AttributeTargets.Propetry)]
if you want to use this attribute in two or more specific position use this:
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]


** allowing a custom attribute to be used multiple time 
ex:
[AttributeUsage(AttributesTarget.All, AllowMultiple = true)] // use multiple time
[AttributeUsage(AttributesTarget.All, AllowMultiple = false)] // don't use multiple time

** Controlling Attribute inheritance
[AttributeUsage(AttributesTarget.All, Inherited = true)] / can iherited with other attributes
[AttributeUsage(AttributesTarget.All, Inherited = false)] // can't use as parent of another attribute
