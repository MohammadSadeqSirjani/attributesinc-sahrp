﻿using System;
using System.Diagnostics;
using System.Text;

namespace ContactWriter
{
    public class ContactConsoleWriter
    {
        private readonly Contact _contact;
        private ConsoleColor _color;

        public ContactConsoleWriter(Contact contact)
        {
            _contact = contact;
        }

        /*
         * [Obsolete]
         * It use for representing other programmer that this(these) method(s) are deprecated in a bit version later
         * It has 3 overloads
         * [Obsolete]
         * [Obsolete(message:string)] // It shows warning with specific message in Compile-time
         * [Obsolete(message:string, error:bool)] //It shows error in Compile-time
         */
        //[Obsolete("This will be removed in version 2.", true)]
        public void Write()
        {
            UseDefaultColor();

            WriteFirstName();

            WriteAge();
        }

        private void WriteAge()
        {
            OutputDebugInfo();
            OutputExtraInfo();
            Console.WriteLine(_contact.AgeInYears);
        }

        private void WriteFirstName()
        {
            var firstNameProperty = typeof(Contact).GetProperty(nameof(Contact.FirstName));
            var firstNameDisplayAttribute = (DisplayAttribute)
                Attribute.GetCustomAttribute(firstNameProperty ?? throw new InvalidOperationException()
                    , typeof(DisplayAttribute));

            var indentAttributes = (IndentAttribute[])
                Attribute.GetCustomAttributes(firstNameProperty, typeof(IndentAttribute));

            PreserveForegroundColor();

            var sb = new StringBuilder();

            foreach (var indent in indentAttributes)
            {
                sb.Append(new string('*', 4));
            }

            Console.ForegroundColor = (ConsoleColor)firstNameDisplayAttribute?.Color;
            sb.Append(firstNameDisplayAttribute?.Label);

            sb.Append(_contact?.FirstName);

            Console.WriteLine(sb);

            RestoreForegroundColor();
        }

        [Conditional("DEBUG")]
        private void OutputDebugInfo()
        {
            Console.WriteLine("***DEBUG MODE***");
        }

        /*
         * It tells the compiler that compile this function in specific mode
         * In normal condition we don't have 'EXTRA' mode in [Conditional]
         * for introducing this condition:
         * go to project properties in build mode
         * type your own condition name in 'Condition compilation symbols' | first choose the DEBUG or RELEASE mode
         */
        [Conditional("EXTRA")]
        private void OutputExtraInfo()
        {
            Console.WriteLine("***EXTRA INFO***");
        }

        private void PreserveForegroundColor()
        {
            _color = Console.ForegroundColor;
        }

        private void RestoreForegroundColor()
        {
            Console.ForegroundColor = _color;
        }

        private void UseDefaultColor()
        {
            var defaultColorAttribute = (DefaultColorAttribute)
                Attribute.GetCustomAttribute(typeof(Contact), typeof(DefaultColorAttribute));

            Console.ForegroundColor = (ConsoleColor)defaultColorAttribute?.Color;
        }
    }
}
