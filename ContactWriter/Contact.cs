﻿using System;
using System.Diagnostics;

namespace ContactWriter
{
    /// <summary>
    /// DebuggerDisplay
    /// Show us specific line of string that represent the debugger value
    /// I mean when you debug the program, it shows you some string that tell you about this class or field
    /// in debug mode
    /// ex:
    /// [DisplayDebugger("In debug mode")]
    /// var field = new Field()
    /// note:
    /// in debug mode field show 'In debug mode' statement
    /// DebuggerTypeProxy
    /// Specific the display proxy for a type
    /// </summary>

    [DebuggerDisplay("First Name = {FirstName} and Age in Years = {AgeInYears}")]
    [DebuggerTypeProxy(typeof(ContactDebugDisplay))]
    [DefaultColor(Color = ConsoleColor.Magenta)]
    public class Contact
    {
        [Display("Firstname: ", color: ConsoleColor.Cyan)]
        [Indent]
        [Indent]
        [Indent]
        [Indent]
        [Indent]
        public string FirstName { get; set; }


        /*
         * DebuggerBrowsable(DebuggerBrowableState.Enum)
         * Enum:
         * - collapse | show element as collapse | default value : 2
         * - never | never show this element | default value : 0
         * - root hidden | don't show this element. Show child elements if it is collection or array 
         */
        /*[DebuggerBrowsable(DebuggerBrowsableState.Never)]*/
        public int AgeInYears { get; set; }
    }
}
