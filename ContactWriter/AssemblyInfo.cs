﻿using System.Reflection;
using System.Runtime.CompilerServices;

/*
 * Use this attributes to change the assembly rules
 * is assembly file we have some items:
 * - AssemblyCopyright
 * - AssemblyCompany
 * - AssemblyConfiguration
 * - AssemblyFileVersion
 * - AssemblyInformationalVersion
 * - AssemblyTitle
 * - AssemblyProduct
 * - ...
 */
[assembly: AssemblyCopyright("Contact Writer @c@ 2020")]
[assembly: InternalsVisibleTo("ContactWriter.Test")]
