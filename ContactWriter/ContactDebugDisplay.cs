﻿namespace ContactWriter
{

    /// <summary>
    /// It helps you to represent new properties in debug mode for specific class or struct
    /// </summary>
    public class ContactDebugDisplay
    {
        private readonly Contact _contact;
        public ContactDebugDisplay(Contact contact)
        {
            _contact = contact;
        }

        public string UpperName => _contact.FirstName.ToUpper();

        public string AgeInHex => _contact.AgeInYears.ToString("X");
    }
}