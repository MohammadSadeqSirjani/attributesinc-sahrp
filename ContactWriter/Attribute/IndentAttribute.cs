﻿using System;

namespace ContactWriter
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class IndentAttribute : Attribute
    {
    }
}
