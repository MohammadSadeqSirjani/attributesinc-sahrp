﻿using System;

namespace ContactWriter
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class DefaultColorAttribute : Attribute
    {
        public ConsoleColor Color { get; set; } = ConsoleColor.Yellow;
    }
}
