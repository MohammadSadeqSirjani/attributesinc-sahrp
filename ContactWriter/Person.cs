﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ContactWriter
{
    [Serializable]  
    public class Person
    {
        [Required] public int Id { get; set; }

        [Required, StringLength(maximumLength: 120)] public string Firstname { get; set; }
        [Required, MaxLength(length: 120)] public string Surname { get; set; }
        [Range(0, 120)] public int AgeInYears { get; set; }
    }
}
