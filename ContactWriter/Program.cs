﻿using System;

namespace ContactWriter
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var sarah = new Contact()
            {
                FirstName = "Sarah",
                AgeInYears = 42
            };

            var sarahWriter = new ContactConsoleWriter(sarah);

            sarahWriter.Write();

            Console.WriteLine("Hit enter to exit ...");
            Console.ReadKey();
        }
    }
}
