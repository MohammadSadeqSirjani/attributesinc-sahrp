﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemAttributes
{
    public static class Student
    {
        [Obsolete("Method1 is obsolete", true)]
        public static void Method1()
        {
            Console.WriteLine("This is Method1");
        }

        public static void Method2()
        {
            Console.WriteLine("This is Method2");
        }
    }
}
