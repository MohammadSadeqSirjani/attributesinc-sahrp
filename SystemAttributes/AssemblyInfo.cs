﻿using System;

/*
 * This attribute shows if a particular code element complies with the Common Language Specification.
 * If a particular code element complies with the Common Language Specification. If it doesn't, then a
 * warning message is issued by the compiler.
 */
[assembly: CLSCompliant(true)]