﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemAttributes
{
    public enum Colors
    {
        Red = 1,
        Blue = 2,
        Pink = 4,
        Green = 8
    }

    /*
     * The FlagsAttribute specifies that an enumeration can be used as a set of flags. This is most commonly
     * used with bitwise operators.
     */
    [Flags]
    public enum FlagColors
    {
        Red = 1,
        Blue = 2,
        Pink = 4,
        Green = 8
    } 
}
